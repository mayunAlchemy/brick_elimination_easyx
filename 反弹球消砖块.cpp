#include <graphics.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include<time.h>
#define row 5
#define colunm 20
#define CMD_UP 1
#define CMD_DOWN 2
#define CMD_LEFT 4
#define CMD_RIGHT 8

int a[row][colunm] = { 0 };
int ballx, bally;//小球的坐标
int boardx, boardy;//木板的坐标
bool starting = false;//判断是否小球正在运动
int addx = 1;
int addy = -1;//小球坐标加的量
bool iscatch = false;//判断是否木板捕捉到小球

void DispatchCommand(int _cmd)
{
	setfillcolor(BLACK);
	solidrectangle(boardx, boardy, boardx + 60, boardy + 10);

	if (_cmd & CMD_LEFT)
	{
		if (iscatch)
		{
			srand(time(NULL));
			addx = rand() % 2 + 1;
			setfillcolor(BLACK);
			solidcircle(ballx, bally, 10);
			ballx -= 10;
			setfillcolor(GREEN);
			solidcircle(ballx, bally, 10);
		}
		boardx -= 5;
	}
	if (_cmd & CMD_RIGHT)
	{
		if (iscatch) {
			srand(time(NULL));
			addx = rand() % 2 + 1;
			setfillcolor(BLACK);
			solidcircle(ballx, bally, 10);
			ballx += 10;
			setfillcolor(LIGHTBLUE);
			solidcircle(ballx, bally, 10);
		}
		boardx += 5;
	}
	if (boardx <= 10)
		boardx = 10;
	if (boardx >= 380)
		boardx = 380;
	setfillcolor(GREEN);
	solidrectangle(boardx, boardy, boardx + 60, boardy + 10);
}
int GetCommand()
{
	int c = 0;
	if (GetAsyncKeyState('A') & 0x8000) c |= CMD_LEFT;
	if (GetAsyncKeyState('D') & 0x8000) c |= CMD_RIGHT;
	if (GetAsyncKeyState('W') & 0x8000) c |= CMD_UP;
	if (GetAsyncKeyState('S') & 0x8000) c |= CMD_DOWN;
	return c;
}
void drawrectangle()
{
	setfillcolor(RED);
	for (int j = 0; j < row; j++)
	{
		for (int i = 0; i < colunm; i++)
		{
			if (a[j][i] == 0)
				solidrectangle(i * 22, j * 12, i * 22 + 20, j * 12 + 10);
		}
	}
}
void startup()
{
	boardx = 190;
	boardy = 590;
	ballx = 220;
	bally = 580;
	drawrectangle();
	setfillcolor(GREEN);
	solidrectangle(boardx, boardy, boardx + 60, boardy + 10);//初始化木板
	setfillcolor(GREEN);
	solidcircle(ballx, bally, 10);//初始化小球
}
void drawball()
{
	iscatch = false;
	setfillcolor(BLACK);//用于擦出上一次的小球
	solidcircle(ballx, bally, 10);
	BeginBatchDraw();
	if (ballx >= 430)
	{
		addx = -1;
	}
	if (ballx <= 0)
	{
		addx = 1;
	}
	for (int i = 0; i < 5; i++)
	{
		int flag0 = 0;
		for (int j = 0; j < 20; j++)
		{
			int currentx = j * 22;
			int currenty = i * 12;
			if (a[i][j] == 0 && ballx <= currentx + 30 && ballx >= currentx - 10 && bally - currenty < 20 && bally >= currenty)//判断小球碰到哪个砖块
			{
				printf("a\n");
				addy = -1 * addy;
				addx = -1 * addx;
				a[i][j] = 1;
				flag0 = 1;
				setfillcolor(BLACK);
				solidrectangle(currentx, currenty, currentx + 20, currenty + 10);
				break;
			}
		}
		if (flag0)
			break;
	}
	if (starting && ballx <= boardx + 70 && ballx >= boardx - 10 && boardy - bally <= 10 && boardy - bally >= 5)//判断木板是否接到小球
	{
		iscatch = true;
		addy = -1 * addy;
	}

	starting = true;
	ballx += addx;//更新小球坐标
	bally += addy;
	setfillcolor(GREEN);
	solidcircle(ballx, bally, 10);
	FlushBatchDraw();
	Sleep(5);
}
void play()
{
	char c;
	while (1)
	{
		drawrectangle();

		if (_kbhit())
		{
			c = GetCommand();
			DispatchCommand(c);

		}

		drawball();

		if (bally > 590)
			break;
	}
}
void main()
{
	initgraph(440, 600, SHOWCONSOLE);
	startup();
	play();
	Sleep(3000);
	cleardevice();
	outtextxy(200, 300, "GAME OVER");
	Sleep(5000);
	_getch();
}